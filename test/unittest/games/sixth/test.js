require = require('@std/esm')(module, { esm: 'mjs', cjs: true });

const OpenXum = require('../../../../lib/openxum-core/').default;

describe('test Color', () => {
    test('color est ', () => {
        expect(OpenXum.Sixth.Color.BLACK).toBe(0);
    });
});

describe('test Phase', () => {
    test('Phase est ', () => {
        expect(OpenXum.Sixth.Phase.FINISH).toBe(1);
    });
});

describe('test1 engine', () => {
    test('test1 engine ' , () => {
        let engine = new OpenXum.Sixth.Engine();
        for (let i = 0 ; i< 5 ; i++){
            for (let j = 0 ; j< 5 ; j++){
                expect(engine._search_tower_index(i,j)).toBe(null);
            }
        }
    });


    test('test2 engine' , () => {
        let engine = new OpenXum.Sixth.Engine();
        for (let i = 0 ; i< 5 ; i++){
            for (let j = 0 ; j< 5 ; j++){
                expect(engine._search_tower(i,j)).toBe(null);
            }
        }
    });

    test('test3 engine' , () => {
        let engine = new OpenXum.Sixth.Engine();
        expect(engine.get_black_piece_remaining()).toBe(15);
        expect(engine.get_white_piece_remaining()).toBe(15);
    });

    test('test4 engine' , () => {
        let engine = new OpenXum.Sixth.Engine();
        let move   = new OpenXum.Sixth.Move(0,{x: 2, y: 1},{x: 2, y: 4},0);
        engine._put_piece(move);
        expect(engine._search_tower(2, 4)).not.toBe(null);
        expect(engine.get_white_piece_remaining()).toBe(14);
    });

    test('test5 engine' , () => {
        let engine = new OpenXum.Sixth.Engine();
        let move1   = new OpenXum.Sixth.Move(0,{x: 2, y: 1},{x: 0, y: 0},0);
        let move2   = new OpenXum.Sixth.Move(0,{x: 2, y: 1},{x: 1, y: 0},0);

        engine._put_piece(move1);
        engine._put_piece(move2);

        expect(engine._search_tower(0, 0)).not.toBe(null);
        expect(engine._search_tower(1, 0)).not.toBe(null);


        let move3   = new OpenXum.Sixth.Move(1,{x: 0, y: 0},{x: 1, y: 0},0);
        engine._move_piece(move3);
        expect(engine._search_tower(0, 0)).toBe(null);
        expect(engine._search_tower(1, 0)).not.toBe(null);
    });

    test('test6 engine' , () => {

        let engine = new OpenXum.Sixth.Engine();

        let move = new OpenXum.Sixth.Move(0, {x: 2, y: 1}, {x: 0, y: 0}, 0);
        engine._put_piece(move);

        expect(engine._search_tower(0, 0)).not.toBe(null);

    });

    test('test7 engine' , () => {
        let engine = new OpenXum.Sixth.Engine();

        let moveP1 = new OpenXum.Sixth.Move(0, {x: -1, y: -1}, {x: 0, y: 0}, 0);
        let moveP2 = new OpenXum.Sixth.Move(0, {x: -1, y: -1}, {x: 0, y: 1}, 0);
        let moveP3 = new OpenXum.Sixth.Move(0, {x: -1, y: -1}, {x: 4, y: 0}, 0);

        engine._put_piece(moveP1);
        engine._put_piece(moveP2);
        engine._put_piece(moveP3);


        let moveM1 = new OpenXum.Sixth.Move(1, {x: 0, y: 1}, {x: 0, y: 0}, 0);
        let moveM2 = new OpenXum.Sixth.Move(1, {x: 0, y: 0}, {x: 4, y: 0}, 0);

        engine._move_piece(moveM1);
        engine._move_piece(moveM2);


        expect(engine._search_tower(4, 0)).not.toBe(null);

    });

    test('test7 engine mvt Tower' , () => {
        let engine = new OpenXum.Sixth.Engine();

        let moveP1 = new OpenXum.Sixth.Move(0, {x: -1, y: -1}, {x: 0, y: 0}, 0);
        let moveP2 = new OpenXum.Sixth.Move(0, {x: -1, y: -1}, {x: 0, y: 1}, 0);
        let moveP3 = new OpenXum.Sixth.Move(0, {x: -1, y: -1}, {x: 4, y: 0}, 0);

        engine._put_piece(moveP1);
        engine._put_piece(moveP2);
        engine._put_piece(moveP3);


        let moveM1 = new OpenXum.Sixth.Move(1, {x: 0, y: 1}, {x: 0, y: 0}, 0);
        let moveM2 = new OpenXum.Sixth.Move(1, {x: 0, y: 0}, {x: 4, y: 0}, 0);

        engine._move_piece(moveM1);
        engine._move_piece(moveM2);

        let indice = engine._search_tower_index(4,0);
        expect(engine._towers[indice].pieces.length).toBe(3);

    });

    test('test8 engine mvt Knight' , () => {
        let engine = new OpenXum.Sixth.Engine();

        let moveP1 = new OpenXum.Sixth.Move(0, {x: -1, y: -1}, {x: 0, y: 0}, 0);
        let moveP2 = new OpenXum.Sixth.Move(0, {x: -1, y: -1}, {x: 0, y: 1}, 0);
        let moveP3 = new OpenXum.Sixth.Move(0, {x: -1, y: -1}, {x: 4, y: 0}, 0);
        let moveP4 = new OpenXum.Sixth.Move(0, {x: -1, y: -1}, {x: 3, y: 2}, 0);
        let moveP5 = new OpenXum.Sixth.Move(0, {x: -1, y: -1}, {x: 4, y: 2}, 0);

        engine._put_piece(moveP1);
        engine._put_piece(moveP2);
        engine._put_piece(moveP3);
        engine._put_piece(moveP4);
        engine._put_piece(moveP5);


        let moveM1 = new OpenXum.Sixth.Move(1, {x: 0, y: 1}, {x: 0, y: 0}, 0);
        let moveM2 = new OpenXum.Sixth.Move(1, {x: 0, y: 0}, {x: 4, y: 0}, 0);
        let moveM3 = new OpenXum.Sixth.Move(1, {x: 4, y: 0}, {x: 3, y: 2}, 0);

        engine._move_piece(moveM1);
        engine._move_piece(moveM2);

        let indice1 = engine._search_tower_index(4,0);
        expect(engine._towers[indice1].pieces.length).toBe(3);


        engine._move_piece(moveM3);
        let indice2 = engine._search_tower_index(3,2);
        expect(engine._towers[indice2].pieces.length).toBe(4);

    });


    test('test9 engine mvt Bishop' , () => {
        let engine = new OpenXum.Sixth.Engine();

        let moveP1 = new OpenXum.Sixth.Move(0, {x: -1, y: -1}, {x: 0, y: 0}, 0);
        let moveP2 = new OpenXum.Sixth.Move(0, {x: -1, y: -1}, {x: 0, y: 1}, 0);
        let moveP3 = new OpenXum.Sixth.Move(0, {x: -1, y: -1}, {x: 4, y: 0}, 0);
        let moveP4 = new OpenXum.Sixth.Move(0, {x: -1, y: -1}, {x: 3, y: 2}, 0);
        let moveP5 = new OpenXum.Sixth.Move(0, {x: -1, y: -1}, {x: 4, y: 2}, 0);
        let moveP6 = new OpenXum.Sixth.Move(0, {x: -1, y: -1}, {x: 1, y: 4}, 0);

        engine._put_piece(moveP1);
        engine._put_piece(moveP2);
        engine._put_piece(moveP3);
        engine._put_piece(moveP4);
        engine._put_piece(moveP5);
        engine._put_piece(moveP6);

        let moveM1 = new OpenXum.Sixth.Move(1, {x: 0, y: 1}, {x: 0, y: 0}, 0);
        let moveM2 = new OpenXum.Sixth.Move(1, {x: 0, y: 0}, {x: 4, y: 0}, 0);
        let moveM3 = new OpenXum.Sixth.Move(1, {x: 4, y: 0}, {x: 3, y: 2}, 0);
        let moveM4 = new OpenXum.Sixth.Move(1, {x: 3, y: 2}, {x: 1, y: 4}, 0);

        engine._move_piece(moveM1);
        engine._move_piece(moveM2);
        engine._move_piece(moveM3);
        engine._move_piece(moveM4);

        let indice = engine._search_tower_index(1,4);
        expect(engine._towers[indice].pieces.length).toBe(5);
        expect(engine._towers[indice].pieces.length).not.toBe(4);

    });


    test('test10 engine mvt Queen' , () => {
        let engine = new OpenXum.Sixth.Engine();

        let moveP1 = new OpenXum.Sixth.Move(0, {x: -1, y: -1}, {x: 0, y: 0}, 0);
        let moveP2 = new OpenXum.Sixth.Move(0, {x: -1, y: -1}, {x: 0, y: 1}, 0);
        let moveP3 = new OpenXum.Sixth.Move(0, {x: -1, y: -1}, {x: 4, y: 0}, 0);
        let moveP4 = new OpenXum.Sixth.Move(0, {x: -1, y: -1}, {x: 3, y: 2}, 0);
        let moveP5 = new OpenXum.Sixth.Move(0, {x: -1, y: -1}, {x: 4, y: 2}, 0);
        let moveP6 = new OpenXum.Sixth.Move(0, {x: -1, y: -1}, {x: 1, y: 4}, 0);
        let moveP7 = new OpenXum.Sixth.Move(0, {x: -1, y: -1}, {x: 4, y: 4}, 0);

        engine._put_piece(moveP1);
        engine._put_piece(moveP2);
        engine._put_piece(moveP3);
        engine._put_piece(moveP4);
        engine._put_piece(moveP5);
        engine._put_piece(moveP6);
        engine._put_piece(moveP7);

        let moveM1 = new OpenXum.Sixth.Move(1, {x: 0, y: 1}, {x: 0, y: 0}, 0);
        let moveM2 = new OpenXum.Sixth.Move(1, {x: 0, y: 0}, {x: 4, y: 0}, 0);
        let moveM3 = new OpenXum.Sixth.Move(1, {x: 4, y: 0}, {x: 3, y: 2}, 0);
        let moveM4 = new OpenXum.Sixth.Move(1, {x: 3, y: 2}, {x: 1, y: 4}, 0);
        let moveM5 = new OpenXum.Sixth.Move(1, {x: 1, y: 4}, {x: 4, y: 4}, 0);

        engine._move_piece(moveM1);
        engine._move_piece(moveM2);
        engine._move_piece(moveM3);
        engine._move_piece(moveM4);
        engine._move_piece(moveM5);

        let indice = engine._search_tower_index(4,4);
        expect(engine._towers[indice].pieces.length).toBe(6);
        expect(engine._towers[indice].pieces.length).not.toBe(5);
        expect(engine._towers[indice].pieces.length).not.toBe(3);
        expect(engine._towers[indice].pieces.length).not.toBe(7);

    });

    test('test11 white gagne' , () => {
        let engine = new OpenXum.Sixth.Engine();

        let moveP1 = new OpenXum.Sixth.Move(0, {x: -1, y: -1}, {x: 0, y: 0}, 0);
        let moveP2 = new OpenXum.Sixth.Move(0, {x: -1, y: -1}, {x: 0, y: 1}, 0);
        let moveP3 = new OpenXum.Sixth.Move(0, {x: -1, y: -1}, {x: 4, y: 0}, 0);
        let moveP4 = new OpenXum.Sixth.Move(0, {x: -1, y: -1}, {x: 3, y: 2}, 0);
        let moveP5 = new OpenXum.Sixth.Move(0, {x: -1, y: -1}, {x: 4, y: 2}, 0);
        let moveP6 = new OpenXum.Sixth.Move(0, {x: -1, y: -1}, {x: 1, y: 4}, 0);
        let moveP7 = new OpenXum.Sixth.Move(0, {x: -1, y: -1}, {x: 4, y: 4}, 0);

        engine._put_piece(moveP1);
        engine._put_piece(moveP2);
        engine._put_piece(moveP3);
        engine._put_piece(moveP4);
        engine._put_piece(moveP5);
        engine._put_piece(moveP6);
        engine._put_piece(moveP7);

        let moveM1 = new OpenXum.Sixth.Move(1, {x: 0, y: 1}, {x: 0, y: 0}, 0);
        let moveM2 = new OpenXum.Sixth.Move(1, {x: 0, y: 0}, {x: 4, y: 0}, 0);
        let moveM3 = new OpenXum.Sixth.Move(1, {x: 4, y: 0}, {x: 3, y: 2}, 0);
        let moveM4 = new OpenXum.Sixth.Move(1, {x: 3, y: 2}, {x: 1, y: 4}, 0);
        let moveM5 = new OpenXum.Sixth.Move(1, {x: 1, y: 4}, {x: 4, y: 4}, 0);

        engine._move_piece(moveM1);
        engine._move_piece(moveM2);
        engine._move_piece(moveM3);
        engine._move_piece(moveM4);
        engine._move_piece(moveM5);

        let indice = engine._search_tower_index(4,4);

        expect(engine.winner_is()).toBe(1);
    });

    test('test12 black lose' , () => {
        let engine = new OpenXum.Sixth.Engine();

        let moveP1 = new OpenXum.Sixth.Move(0, {x: -1, y: -1}, {x: 0, y: 0}, 0);
        let moveP2 = new OpenXum.Sixth.Move(0, {x: -1, y: -1}, {x: 0, y: 1}, 0);
        let moveP3 = new OpenXum.Sixth.Move(0, {x: -1, y: -1}, {x: 4, y: 0}, 0);
        let moveP4 = new OpenXum.Sixth.Move(0, {x: -1, y: -1}, {x: 3, y: 2}, 0);
        let moveP5 = new OpenXum.Sixth.Move(0, {x: -1, y: -1}, {x: 4, y: 2}, 0);
        let moveP6 = new OpenXum.Sixth.Move(0, {x: -1, y: -1}, {x: 1, y: 4}, 0);
        let moveP7 = new OpenXum.Sixth.Move(0, {x: -1, y: -1}, {x: 4, y: 4}, 0);

        engine._put_piece(moveP1);
        engine._put_piece(moveP2);
        engine._put_piece(moveP3);
        engine._put_piece(moveP4);
        engine._put_piece(moveP5);
        engine._put_piece(moveP6);
        engine._put_piece(moveP7);

        let moveM1 = new OpenXum.Sixth.Move(1, {x: 0, y: 1}, {x: 0, y: 0}, 0);
        let moveM2 = new OpenXum.Sixth.Move(1, {x: 0, y: 0}, {x: 4, y: 0}, 0);
        let moveM3 = new OpenXum.Sixth.Move(1, {x: 4, y: 0}, {x: 3, y: 2}, 0);
        let moveM4 = new OpenXum.Sixth.Move(1, {x: 3, y: 2}, {x: 1, y: 4}, 0);
        let moveM5 = new OpenXum.Sixth.Move(1, {x: 1, y: 4}, {x: 4, y: 4}, 0);

        engine._move_piece(moveM1);
        engine._move_piece(moveM2);
        engine._move_piece(moveM3);
        engine._move_piece(moveM4);
        engine._move_piece(moveM5);

        let indice = engine._search_tower_index(4,4);

        expect(engine.winner_is()).not.toBe(0);
    });

    test('test13  mouvements possibles' , () => {
        let engine = new OpenXum.Sixth.Engine();

        let moveP1 = new OpenXum.Sixth.Move(0, {x: -1, y: -1}, {x: 0, y: 0}, 0);
        let moveP2 = new OpenXum.Sixth.Move(0, {x: -1, y: -1}, {x: 0, y: 1}, 0);


        engine._put_piece(moveP1);
        engine._put_piece(moveP2);

        let list_mouvement = [];
        let liste = engine.get_possible_move_list();

        for (let i = 0 ; i< liste.length ; i++){
            if ( (liste[i].get_from().x === 0 ) && ( liste[i].get_from().y===0) ){
                list_mouvement=list_mouvement.concat(liste[i]);
            }
        }
        expect(list_mouvement.length).toBe(1);
    });

    test('test14  mouvements possibles' , () => {
        let engine = new OpenXum.Sixth.Engine();

        let moveP1 = new OpenXum.Sixth.Move(0, {x: -1, y: -1}, {x: 0, y: 0}, 0);
        let moveP2 = new OpenXum.Sixth.Move(0, {x: -1, y: -1}, {x: 0, y: 1}, 0);
        let moveP3 = new OpenXum.Sixth.Move(0, {x: -1, y: -1}, {x: 4, y: 0}, 0);
        let moveP4 = new OpenXum.Sixth.Move(0, {x: -1, y: -1}, {x: 0, y: 4}, 0);

        engine._put_piece(moveP1);
        engine._put_piece(moveP2);
        engine._put_piece(moveP3);
        engine._put_piece(moveP4);


        let move= new OpenXum.Sixth.Move(1, {x: 0, y: 1}, {x: 0, y: 0}, 0);
        engine._move_piece(move);

        let list_mouvement = [];
        let liste = engine.get_possible_move_list();
        for (let i = 0 ; i< liste.length ; i++){
            if ( (liste[i].get_from().x === 0 ) && ( liste[i].get_from().y===0) ){
                list_mouvement=list_mouvement.concat(liste[i]);
            }
        }
        expect(list_mouvement.length).toBe(2);
        expect(list_mouvement.length).not.toBe(3);
        expect(list_mouvement.length).not.toBe(1);
    });

});