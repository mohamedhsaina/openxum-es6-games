require = require('@std/esm')(module, {esm: 'mjs', cjs: true});

const OpenXum = require('../../../lib/openxum-core/').default;
const AI = require('../../../lib/openxum-ai/').default;
let cptWhite = 0 ;
let cptBlack = 0 ;
for(let i=0;i<=100;i++){
let e = new OpenXum.Sixth.Engine(OpenXum.Sixth.GameType.STANDARD,OpenXum.Sixth.Color.WHITE);

let p1 = new AI.Generic.RandomPlayer(OpenXum.Sixth.Color.BLACK, OpenXum.Sixth.Color.WHITE, e);
let p2 = new AI.Specific.Sixth.AlphaBetaPlayer(OpenXum.Sixth.Color.WHITE,OpenXum.Sixth.Color.BLACK, e);
let p = p1;



while (!e.is_finished()) {
    let move = p.move();

    if (move.constructor === Array) {
        for (let i = 0; i < move.length; ++i) {
            console.log(move[i].to_string());
        }
    } else {
        console.log(move.to_string());
    }
    e.move(move);
    p = p === p1 ? p2 : p1;
}
if (e.winner_is() === OpenXum.Sixth.Color.BLACK) {
    cptBlack++;
    console.log("Winner is black");
} else {
    if (e.winner_is() === OpenXum.Sixth.Color.WHITE) {
        cptWhite++;
        console.log("Winner is white");
    }
    else{
        console.log("No winner");
    }
}}

console.log("black win : " + cptBlack);
console.log("white win : " + cptWhite);
