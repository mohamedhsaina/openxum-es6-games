require = require('@std/esm')(module, {esm: 'mjs', cjs: true});

const OpenXum = require('../../../lib/openxum-core/').default;
const AI = require('../../../lib/openxum-ai/').default;

let black_win = 0;
let white_win = 0;

for (let i = 0; i < 1; ++i) {
  let e = new OpenXum.Hnefatafl.Engine(OpenXum.Hnefatafl.GameType.STANDARD, OpenXum.Hnefatafl.Color.BLACK);
  let p1 = new AI.Generic.RandomPlayer(OpenXum.Hnefatafl.Color.BLACK, OpenXum.Hnefatafl.Color.WHITE, e);
  let p2 = new AI.Generic.Hnefatafl.IA.IAHnefataflPlayer(OpenXum.Hnefatafl.Color.WHITE, OpenXum.Hnefatafl.Color.BLACK, e);
  let p = p1;
  
  while (!e.is_finished()) {
    e.move(p.move());
    p = p === p1 ? p2 : p1;
  }

  console.log("Winner is " + (e.winner_is() === OpenXum.Hnefatafl.Color.BLACK ? "black" : "white"));
  if (e.winner_is() === OpenXum.Hnefatafl.Color.BLACK) {
    black_win++;
  } else {
    white_win++;
  }
}

console.log("Black wins: " + black_win);
console.log("White wins: " + white_win);