"use strict";

import OpenXum from '../../openxum/manager.mjs';
import Sixth from '../../../openxum-core/games/sixth/index.mjs';

class Manager extends OpenXum.Manager {
    constructor(t, e, g, o, s, f) {
        super(t, e, g, o, s, f);
        this.that(this);
    }

    build_move() {
        return new Sixth.Move();
    }

    get_current_color() {
        return this.engine().current_color() === Sixth.Color.WHITE ? 'White' : 'Black';
    }

    static get_name() {
        return 'Sixth';
    }

    get_winner_color() {
        console.log(this.engine().winner_is());
        if (this.engine().winner_is() === Sixth.Color.WHITE) {
            return 'White';
        } else if (this.engine().winner_is() === Sixth.Color.BLACK) {
            return 'Black';
        }
        return 'Nobody';
    }

    process_move() {
    }
}

export default Manager;