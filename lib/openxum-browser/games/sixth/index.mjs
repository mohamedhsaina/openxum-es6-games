"use strict";

import Sixth from '../../../openxum-core/games/sixth/index.mjs';
import AI from '../../../openxum-ai/generic/index.mjs';
import Gui from './gui.mjs';
import Manager from './manager.mjs';

export default {
    Gui: Gui,
    Manager: Manager,
    Settings: {
        ai: {
            mcts: AI.RandomPlayer // TODO: MCTSPlayer
        },
        colors: {
            first: Sixth.Color.BLACK,
            init: Sixth.Color.BLACK,
            list: [
                {key: Sixth.Color.BLACK, value: 'black'},
                {key: Sixth.Color.WHITE, value: 'white'}
            ]
        },
        modes: {
            init: Sixth.GameType.STANDARD,
            list: [
                {key: Sixth.GameType.STANDARD, value: 'standard'}
            ]
        },
        opponent_color(color) {
            return color === Sixth.Color.BLACK ? Sixth.Color.WHITE : Sixth.Color.BLACK;
        },
        types: {
            init: 'ai',
            list: [
                {key: 'gui', value: 'GUI'},
                {key: 'ai', value: 'AI'},
                {key: 'online', value: 'Online'},
                {key: 'offline', value: 'Offline'}
            ]
        }
    }
};