"use strict";

import Color from './color.mjs';
import Move from './move.mjs';
import OpenXum from '../../openxum/index.mjs';
import Phase from './phase.mjs';
import MoveType from './move_type.mjs';

class Engine extends OpenXum.Engine {

    constructor() {
        super();
        this._color = Color.WHITE;
        this._nb_black_piece = 15;
        this._nb_white_piece = 15;
        this._towers = [];
        this._move_to_avoid = null;
        this._phase = Phase.MOVE_TOWER;
        this._winner_color = Color.NONE;
    }

    build_move() {
        return new Move();
    }


    clone() {
        let o = new Engine(this._type);
        o._color = this._color;
        o._nb_black_piece = this._nb_black_piece;
        o._nb_white_piece = this._nb_white_piece;
        let nTowers = new Array(this._towers.length);
        for (let i = 0; i < nTowers.length; i++) {
            let npieces = new Array(this._towers[i].pieces.length);
            for (let j = 0; j < npieces.length; j++) {
                npieces[j] = this._towers[i].pieces[j];
            }
            nTowers[i] = {x: this._towers[i].x, y: this._towers[i].y, pieces: npieces};
        }
        o._towers = nTowers;
        o._move_to_avoid = this._move_to_avoid;
        o._phase = this._phase;
        return o;
    }

    current_color() {
        return this._color;
    }

    current_color_string() {
        return this._color === Color.BLACK ? 'Black' : 'White';
    }

    get_name() {
        return 'SIXTH';
    }

    get_possible_move_list() {
        let list = [];
        if ((this._color === Color.BLACK && this._nb_black_piece > 0) || (this._color === Color.WHITE && this._nb_white_piece > 0)) {
            list = list.concat(this._get_possible_putting_list());
        }
        for (let i = 0; i < this._towers.length; i++) {
            const dest_tower = this._towers[i];
            list = list.concat(this._get_possible_moves_to_tower_list(dest_tower));
        }
        return list;
    }

    get_towers() {
        return this._towers;
    }

    get_black_piece_remaining() {
        return this._nb_black_piece;
    }

    get_white_piece_remaining() {
        return this._nb_white_piece;
    }

    is_finished() {
        return this._phase === Phase.FINISH;
    }

    move(move) {
        if (move.get_type() === MoveType.PUT_PIECE) {
            this._put_piece(move);
        } else {
            this._move_piece(move);
        }

        this._next_color();

        if (this.get_possible_move_list().length === 0) {
            this._phase = Phase.FINISH;
        }
    }


    parse(str) {

    }

    phase() {
        return this._phase;
    }

    to_string() {

    }

    verify_move(move) {
        const list = this.get_possible_move_list();
        for (let i = 0; i < list.length; i++) {
            if (move.is_equal(list[i])) {
                return true;
            }
        }
        return false;
    }

    winner_is() {
        return this._winner_color;
    }

    _get_moves_between_towers(start_tower, dest_tower) {
        let moves_between_towers = [];
        for (let j = 0; j < dest_tower.pieces.length; j++) {
            const move = new Move(MoveType.MOVE_TOWER, {x: start_tower.x, y: start_tower.y}, {
                x: dest_tower.x,
                y: dest_tower.y
            }, j);
            if (!this._move_to_avoid || !move.is_equal(this._move_to_avoid)) {
                moves_between_towers = moves_between_towers.concat(move);
            }
        }
        return moves_between_towers;

    }


    _get_possible_moves_in_direction(start_tower, DirectionX, DirectionY) {

        let move_list = [];
        if (start_tower.x + DirectionX >= 0 && start_tower.x + DirectionX < 5 && (start_tower.y + DirectionY >= 0 && start_tower.y + DirectionY < 5) ){
            if (this._search_tower(start_tower.x + DirectionX, start_tower.y + DirectionY)) {
                let dest_tower = this._search_tower(start_tower.x + DirectionX, start_tower.y + DirectionY);
                if (dest_tower !== null) {
                    move_list = move_list.concat(this._get_moves_between_towers(start_tower, dest_tower));
                }
            }
        }
        return move_list;
    }

    _get_possible_moves_to_tower_list(start_tower) {
        let move_list = [];
        let j = this._search_tower_index(start_tower.x, start_tower.y);
        if (this._towers[j].pieces.length===1) {
            for (let DirectionX = -1; DirectionX <= 1; DirectionX++) {
                for (let DirectionY = -1; DirectionY <= 1; DirectionY++) {
                    if ((DirectionX === 0 || DirectionY === 0) && !(DirectionX === 0 && DirectionY === 0)) {
                        move_list = move_list.concat(this._get_possible_moves_in_direction(start_tower, DirectionX, DirectionY));
                    }
                }
            }
        } else if (this._towers[j].pieces.length===2) {
            for (let DirectionX = -4; DirectionX <= 4; DirectionX++) {
                for (let DirectionY = -4; DirectionY <= 4; DirectionY++) {
                    if ((DirectionX === 0 || DirectionY === 0) && !(DirectionX === 0 && DirectionY === 0)) {
                        move_list = move_list.concat(this._get_possible_moves_in_direction(start_tower, DirectionX, DirectionY));
                    }
                }
            }
        } else if (this._towers[j].pieces.length===3) {
            for (let DirectionX = -4; DirectionX <= 4; DirectionX++) {
                for (let DirectionY = -4; DirectionY <= 4; DirectionY++) {
                    if ((DirectionX === 2 && DirectionY === -1) || (DirectionX === -1 && DirectionY === -2) || (DirectionX === 1 && DirectionY === -2) || (DirectionX === -1 && DirectionY === 2) || (DirectionX === 2 && DirectionY === 1) || (DirectionX === -2 && DirectionY === -1) || (DirectionX === -2 && DirectionY === 1) || (DirectionX === 1  && DirectionY === 2 ) )
                        move_list = move_list.concat(this._get_possible_moves_in_direction(start_tower, DirectionX, DirectionY));
                    }
                }
            }
        else if (this._towers[j].pieces.length===4) {
            for (let DirectionX = -4; DirectionX <= 4; DirectionX++) {
                for (let DirectionY = -4; DirectionY <= 4; DirectionY++) {
                    if (((DirectionX === DirectionY) || (DirectionX === -DirectionY) ) && ((DirectionX !==0 ) && (DirectionY !==0 ))) {
                        move_list = move_list.concat(this._get_possible_moves_in_direction(start_tower, DirectionX, DirectionY));
                    }
                }
            }
        }else if (this._towers[j].pieces.length===5) {
            for (let DirectionX = -4; DirectionX <= 4; DirectionX++) {
                for (let DirectionY = -4; DirectionY <= 4; DirectionY++) {
                    if (((((DirectionX === DirectionY) || (DirectionX === -DirectionY) ) && ((DirectionX !==0 ) && (DirectionY !==0 ))) || (DirectionX === 0 || DirectionY === 0) && !(DirectionX === 0 && DirectionY === 0))) {
                        move_list = move_list.concat(this._get_possible_moves_in_direction(start_tower, DirectionX, DirectionY));
                    }
                }
            }
        }
        return move_list;
    }

    _get_possible_putting_list() {
        let putting_list = [];
        for (let i = 0; i < 5; i++) {
            for (let j = 0; j < 5; j++) {
                if (!this._search_tower(i, j)) {
                    putting_list = putting_list.concat(new Move(MoveType.PUT_PIECE, {x: -1, y: -1}, {x: i, y: j}, 0));
                }
            }
        }
        return putting_list;
    }

    _move_piece(move) {
        let index_tower_from = this._search_tower_index(move.get_from().x, move.get_from().y);
        let index_tower_to = this._search_tower_index(move.get_to().x, move.get_to().y);
        this._move_to_avoid = new Move(move.get_type(), move.get_to(), move.get_from(), this._towers[index_tower_to].pieces.length);
        this._move_to_avoid = null;
        this._process_move(move, index_tower_from, index_tower_to);

    }


    _next_color() {
        this._color = this._color === Color.WHITE ? Color.BLACK : Color.WHITE;
    }

    _process_move(move, index_tower_from, index_tower_to) {
        let to_move = this._towers[index_tower_from].pieces.slice(move.get_piece());
        this._towers[index_tower_to].pieces = this._towers[index_tower_to].pieces.concat(to_move);
        const tower_length_to = this._towers[index_tower_to].pieces.length;
        if (tower_length_to >= 6) {
            this._phase = Phase.FINISH;
            this._winner_color = this._towers[index_tower_to].pieces[tower_length_to - 1];
        }
        if (move.get_piece() === 0) {
            this._remove_tower(index_tower_from);
        } else {
            this._towers[index_tower_from].pieces.splice(move.get_piece());
        }
    }

    _put_piece(move) {
        console.log(move);
        let new_tower = {x: move.get_to().x, y: move.get_to().y, pieces: [this._color]};
        this._towers = this._towers.concat(new_tower);
        this._remove_piece_from_reserve();
        this._move_to_avoid = null;
    }

    _remove_piece_from_reserve() {
        if (this._color === Color.BLACK) {
            this._nb_black_piece--;
        } else {
            this._nb_white_piece--;
        }
    }

    _remove_tower(index_tower_to_remove) {
        this._towers.splice(index_tower_to_remove, 1);
    }

    _search_tower(x, y) {
        for (let i = 0; i < this._towers.length; i++) {
            if (this._towers[i].x === x && this._towers[i].y === y) {
                return this._towers[i];
            }
        }
        return null;
    }

    _search_tower_index(x, y) {
        for (let i = 0; i < this._towers.length; i++) {
            if (this._towers[i].x === x && this._towers[i].y === y) {
                return i;
            }
        }
        return null;
    }
}

export default Engine;