"use strict";

import Color from './color.mjs';
import State from './state.mjs';

class Intersection {
  constructor(c) {
    this._coordinates = c;
    this._state = State.VACANT;
  }

// public methods
  clone() {
    let intersection = new Intersection(this._coordinates.clone());

    intersection.set(this._state);
    return intersection;
  }

  color() {
    if (this._state === State.VACANT) {
      return -1;
    }

    if (this._state === State.BLACK_RING ||
      this._state === State.BLACK_MARKER ||
      this._state === State.BLACK_MARKER_RING) {
      return Color.BLACK;
    } else {
      return Color.WHITE;
    }
  }

  coordinates() {
    return this._coordinates;
  }

  flip() {
    if (this._state === State.BLACK_MARKER) {
      this._state = State.WHITE_MARKER;
    } else if (this._state === State.WHITE_MARKER) {
      this._state = State.BLACK_MARKER;
    }
  }

  hash() {
    return this._coordinates.hash();
  }

  letter() {
    return this._coordinates.letter();
  }

  number() {
    return this._coordinates.number();
  }

  put_marker(color) {
    if (color === Color.BLACK) {
      if (this._state === State.BLACK_RING) {
        this._state = State.BLACK_MARKER_RING;
      }
    } else {
      if (this._state === State.WHITE_RING) {
        this._state = State.WHITE_MARKER_RING;
      }
    }
  }

  put_ring(color) {
    if (color === Color.BLACK) {
      this._state = State.BLACK_RING;
    } else {
      this._state = State.WHITE_RING;
    }
  }

  remove_marker() {
    this._state = State.VACANT;
  }

  remove_ring() {
    if (this._state === State.BLACK_MARKER_RING || this._state === State.WHITE_MARKER_RING) {
      if (this._state === State.BLACK_MARKER_RING) {
        this._state = State.BLACK_MARKER;
      } else {
        this._state = State.WHITE_MARKER;
      }
    }
  }

  remove_ring_board() {
    this._state = State.VACANT;
  }

  state() {
    return this._state;
  }

  set(state) {
    this._state = state;
  }
}

export default Intersection;