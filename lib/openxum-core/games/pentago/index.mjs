"use strict";

// namespace Pentago
import Board from './board.mjs';
import BoardMap from './board_map.mjs';
import Color from './color.mjs';
import Coordinates from './coordinates.mjs';
import Direction from './direction.mjs';
import Engine from './engine.mjs';
import GameType from './game_type.mjs';
import Move from './move.mjs';
import MoveType from './move_type.mjs';
import Phase from './phase.mjs';
import Pivot from './pivot.mjs';
import State from './state.mjs';

export default {
  Board: Board,
  BoardMap: BoardMap,
  Color: Color,
  Coordinates: Coordinates,
  Direction: Direction,
  Engine: Engine,
  GameType: GameType,
  Move: Move,
  MoveType: MoveType,
  Phase: Phase,
  Pivot: Pivot,
  State: State
};