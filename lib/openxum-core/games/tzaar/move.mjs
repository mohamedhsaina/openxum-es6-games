"use strict";

import Color from './color.mjs';
import Coordinates from './coordinates.mjs';
import MoveType from './move_type.mjs';
import OpenXum from '../../openxum/index.mjs';
import Phase from './phase.mjs';

class Move extends OpenXum.Move {
  constructor(t, c, f, to, choice) {
    super();
    this._type = t;
    this._color = c;
    this._from = f;
    this._to = to;
    this._choice = choice;
  }

// public methods
  choice() {
    return this._choice;
  }

  color() {
    return this._color;
  }

  decode(str) {
    const type = str.substring(0, 2);

    if (type === 'FM') {
      this._type = MoveType.FIRST_MOVE;
      this._from = new Coordinates(str.charAt(3), parseInt(str.charAt(4)));
      this._to = new Coordinates(str.charAt(5), parseInt(str.charAt(6)));
    } else if (type === 'Ca') {
      this._type = MoveType.CAPTURE;
      this._from = new Coordinates(str.charAt(3), parseInt(str.charAt(4)));
      this._to = new Coordinates(str.charAt(5), parseInt(str.charAt(6)));
    } else if (type === 'Ch') {
      const choice = str.substring(2, 4);

      this._type = MoveType.CHOOSE;
      if (choice === 'Ca') {
        this._choice = Phase.SECOND_CAPTURE;
      } else if (choice === 'MS') {
        this._choice = Phase.MAKE_STRONGER;
      } else {
        this._choice = Phase.PASS;
      }
    } else if (type === 'SC') {
      this._type = MoveType.SECOND_CAPTURE;
      this._from = new Coordinates(str.charAt(3), parseInt(str.charAt(4)));
      this._to = new Coordinates(str.charAt(5), parseInt(str.charAt(6)));
    } else if (type === 'MS') {
      this._type = MoveType.MAKE_STRONGER;
      this._from = new Coordinates(str.charAt(3), parseInt(str.charAt(4)));
      this._to = new Coordinates(str.charAt(5), parseInt(str.charAt(6)));
    }
  }

  encode() {
    if (this._type === MoveType.FIRST_MOVE) {
      return 'FM' + (this._color === Color.BLACK ? 'B' : 'W') + this._from.to_string() + this._to.to_string();
    } else if (this._type === MoveType.CAPTURE) {
      return 'Ca' + (this._color === Color.BLACK ? 'B' : 'W') + this._from.to_string() + this._to.to_string();
    } else if (this._type === MoveType.CHOOSE) {
      return 'Ch' + this._choice === Phase.SECOND_CAPTURE ? 'Ca' : this._choice === Phase.MAKE_STRONGER ? 'MS' : 'Pa';
    } else if (this._type === MoveType.SECOND_CAPTURE) {
      return 'SC' + (this._color === Color.BLACK ? 'B' : 'W') + this._from.to_string() + this._to.to_string();
    } else if (this._type === MoveType.MAKE_STRONGER) {
      return 'MS' + (this._color === Color.BLACK ? 'B' : 'W') + this._from.to_string() + this._to.to_string();
    }
  }

  from() {
    return this._from;
  }

  from_object(data) {
    this._type = data.type;
    this._color = data.color;
    this._from = new Coordinates(data.from.letter, data.from.number);
    this._to = new Coordinates(data.to.letter, data.to.number);
    this._choice = data.choice;
  }

  to() {
    return this._to;
  }

  to_object() {
    return {
      type: this._type,
      color: this._color,
      from: this._from === null ? {letter: -1, number: -1} : {letter: this._from._letter, number: this._from._number},
      to: this._to === null ? {letter: -1, number: -1} : {letter: this._to._letter, number: this._to._number},
      choice: this._choice
    };
  }

  to_string() {
    if (this._type === MoveType.FIRST_MOVE) {
      return 'Move ' + (this._color === Color.BLACK ? 'black' : 'white') + ' piece from ' + this._from.to_string() + ' to ' + this._to.to_string();
    } else if (this._type === MoveType.CAPTURE) {
      return 'Capture with ' + (this._color === Color.BLACK ? 'black' : 'white') + ' piece from ' +
        this._from.to_string() + ' to ' + this._to.to_string();
    } else if (this._type === MoveType.CHOOSE) {
      return 'Choose ' + this._choice === Phase.SECOND_CAPTURE ? 'to capture' : this._choice === Phase.MAKE_STRONGER ? 'to make stronger' : 'to pass';
    } else if (this._type === MoveType.SECOND_CAPTURE) {
      return 'Capture with ' + (this._color === Color.BLACK ? 'black' : 'white') + ' piece from ' + this._from.to_string() + ' to ' + this._to.to_string();
    } else if (this._type === MoveType.MAKE_STRONGER) {
      return 'Make stronger with ' + (this._color === Color.BLACK ? 'black' : 'white') + ' piece from ' + this._from.to_string() + ' to ' + this._to.to_string();
    }
  }

  type() {
    return this._type;
  }
}

export default Move;