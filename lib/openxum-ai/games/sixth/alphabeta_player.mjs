"use strict";

import AlphaBeta from '../../generic/alphabeta_player.mjs';
import Color from '../../../openxum-core/games/sixth/color.mjs';

class AlphaBetaPlayer extends AlphaBeta {
    constructor(c, o, e) {
        super(c, o, e, 1, 1000);

    }
    evaluate(e, depth) {
        let score = 0;
        let up = 100;
        let color =Color.WHITE;

        if (e.is_finished()) {
            return e.winner_is();
        }
        else {
            for (let i = 0; i < e._towers.length; i++) {
                if (e._towers[i].pieces.length>=1) {
                    score = e._towers[i].pieces.length * up;
                    if(e._towers[i].pieces.Color===color){
                        score += 3*up;
                    }
                }
            }
            return score;
        }
    }
}
export default AlphaBetaPlayer;